<?php

namespace App\Http\Controllers\Api;

use App\Models\Claims;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Helper;

class ClaimController extends Controller
{

    public function index() {
        $claim = Claims::with('status')->get();

        return array([
            'success' => true,
            'claim' => $claim
        ]);
    }

    public function addClaim(Request $request) {

        $id = Claims::create([
            'name' => $request['nama'],
            'ic' => $request['ic'],
            'phoneNum' => $request['phoneNum'],
            'claim_type' => $request['ctype'],
            'descriptions' => $request['descriptions'],
            'status_id' => 1,
            'date' => $request['date'],
        ]);
        return array([
            'success' => true,
        ]);
    }

    public function detail(Request $request) {
        $claim = Claims::where('id', $request['id'])->with('status')->get();

        return array([
            'success' => true,
            'claim' => $claim
        ]);
    }

}
