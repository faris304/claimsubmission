<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Claims extends Model
{
    protected $fillable = [
        'name', 'ic', 'phoneNum','claim_type','descriptions','date','status_id','created_at','updated_at'
    ];
    protected $table = 'claims';

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

}
