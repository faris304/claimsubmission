<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Claim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('status');
			$table->timestamps();
		});

        Schema::create('claims', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 200);
			$table->string('ic');
			$table->string('phoneNum');
			$table->string('claim_type');
			$table->string('descriptions');
            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')
            ->references('id')
            ->on('status')
            ->onUpdate('cascade')
            ->onDelete('cascade');
			$table->dateTime('date');
			$table->timestamps();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
        Schema::dropIfExists('status');

    }
}
