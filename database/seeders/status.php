<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('status')->delete();

        \DB::table('status')->insert(array (
            0 =>
            array (
                'id' => 1,
                'status' => 'Submitted',
            ),
            1 =>
            array (
                'id' => 2,
                'status' => 'Inprogress',
            ),
            2 =>
            array (
                'id' => 3,
                'status' => 'Completed',
            ),
            3 =>
            array (
                'id' => 4,
                'status' => 'Failed',
            ),
        ));
    }
}
