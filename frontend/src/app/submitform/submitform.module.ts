import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmitformRoutingModule } from './submitform-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SubmitformRoutingModule
  ]
})
export class SubmitformModule { }
