import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServicesService } from '../api-services.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-submitform',
  templateUrl: './submitform.component.html',
  styleUrls: ['./submitform.component.css']
})
export class SubmitformComponent {
  public myForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public api: ApiServicesService,
    private router: Router,
    ){
      this.myForm = this.formBuilder.group({
        nama: ['', [Validators.required]],
        ic: ['', [Validators.required]],
        phoneNum: ['', [Validators.required]],
        ctype: ['', [Validators.required]],
        descriptions: ['', [Validators.required]],
        date: ['', [Validators.required]]
      });
    }

  save(){
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    this.api.addClaim(this.myForm.value).then(() => {
      Swal.close();
      this.router.navigate(['/']);
    }).catch(()=>{
      Swal.close();
    });
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
