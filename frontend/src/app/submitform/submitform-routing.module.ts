import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubmitformComponent } from './submitform.component';

const routes: Routes = [
  {
    path: 'submitform',
    component: SubmitformComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmitformRoutingModule { }
