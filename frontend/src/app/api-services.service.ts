import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  addClaim(addData: any){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: 'http://localhost:8000/api/claim',
        data: addData
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  getClaim(){
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'get',
        url: 'http://localhost:8000/api/claim',
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

  getDetail(addData: any){
    console.log(addData);
    return new Promise<object>((resolve, reject) => {
      axios({
        method: 'post',
        url: 'http://localhost:8000/api/detail',
        data: addData
      }).then(async (response: any) =>{
        resolve(response);
      }).catch(()=>{
        reject(true);
      });
    });
  }

}
