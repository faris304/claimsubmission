import { Component } from '@angular/core';
import { ApiServicesService } from '../api-services.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
    public claim: any;
    public empty = false;
    public Notempty = true;
    constructor(
      public api: ApiServicesService,
    ){
      Swal.fire({
        title: 'Loading...',
        timerProgressBar: true,
        didOpen: () => {
          Swal.showLoading();
        }
      });
      this.api.getClaim().then((response: any) => {
        this.claim = response.data[0].claim;
        if (this.claim[0] == undefined) {
          let coll = <HTMLElement>document.getElementById('emptycol');
          coll.style.display = "table-row";
        } else {
          let coll = <HTMLElement>document.getElementById('emptycol');
          coll.style.display = "none";

        }
        Swal.close();
      }).catch(()=>{
        Swal.close();
      })
  }

}
