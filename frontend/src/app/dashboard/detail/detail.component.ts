import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Route } from '@angular/router';
import { formToJSON } from 'axios';
import { ApiServicesService } from 'src/app/api-services.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent {
  public id: any;
  public detail: any;
  public name: any;
  public ic: any;
  public phoneNum: any;
  public claim_type: any;
  public descriptions: any;
  public status: any;
  public date: any;
  constructor(
    private route: ActivatedRoute,
    public api: ApiServicesService,
  ){
    Swal.fire({
      title: 'Loading...',
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      }
    });
    this.route.paramMap.subscribe((params: any) => {
      this.id = params.get('id');
      const data = {
        id: this.id,
      };
      this.api.getDetail(data).then((result: any)=>{
        console.log(result);
        this.name = result.data[0].claim[0].name;
        this.ic = result.data[0].claim[0].ic;
        this.phoneNum = result.data[0].claim[0].phoneNum;
        this.claim_type = result.data[0].claim[0].claim_type;
        this.descriptions = result.data[0].claim[0].descriptions;
        this.status = result.data[0].claim[0].status.status;
        this.date = result.data[0].claim[0].date;
        Swal.close();
      });
    });
  }
}
